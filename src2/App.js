import React from 'react';
import logo from './logo.svg';
import './App.css';

import { Button, WhiteSpace } from 'antd-mobile';

import styled from './css.module.css'
import styled2 from './less.module.less'



function App() {
  return (
    <div className="App">
    <div className={styled.h1}>测试css模块化</div>
    <div className={styled2.t1}>测试less模块化</div>
    <Button>default</Button><WhiteSpace />
    <Button disabled>default disabled</Button><WhiteSpace />

    <Button type="primary">primary</Button><WhiteSpace />
    <Button type="primary" disabled>primary disabled</Button><WhiteSpace />

    <Button type="warning">warning</Button><WhiteSpace />
    <Button type="warning" disabled>warning disabled</Button><WhiteSpace />



      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
