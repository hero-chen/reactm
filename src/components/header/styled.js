import styled from 'styled-components'

export const HeaderDiv = styled.div`
    width: 100%;
    height: 44px;
    color: #ffcd32;
    text-align: center;
    font-size: 18px;
    display: flex;
    justify-content: center;
    .icon{
        width: 30px;
        height: 32px;
        margin: 6px 9px 0 0;
        background-size: 30px 32px;
        background-image: url(/icon.png);
    }
    h1{
        line-height: 44px;
        font-weight: 400;
    }
`