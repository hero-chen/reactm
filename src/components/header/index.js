import React, { Component } from 'react'

// 导入样式
import { HeaderDiv } from './styled'

class HeaderIndex extends Component {
 render() {
     return (
         <HeaderDiv>
             <div className="icon"></div>
             <h1>Chicken Music</h1>
         </HeaderDiv>
     )
 }
}

export default HeaderIndex