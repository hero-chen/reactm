import styled from 'styled-components'

export const LbtDiv = styled.div`
    width: 100%;
    height: 1.5rem;
    background: pink;
    .swiper-pagination-bullets{
        bottom: -0.16rem;
    }
`