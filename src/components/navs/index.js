import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

// 导入样式
import { NavsDiv } from './styled'

class Navs extends Component {
 render() {
     return (
         <NavsDiv>
            <NavLink to="/recommend"><span>推荐</span></NavLink>
            <NavLink to="/singer"><span>歌手</span></NavLink>
            <NavLink to="/rank"><span>排行</span></NavLink>
            <NavLink to="/search"><span>搜索</span></NavLink>
         </NavsDiv>
     )
 }
}

export default Navs