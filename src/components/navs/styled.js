import styled from 'styled-components'

export const NavsDiv = styled.div`
    width: 100%;
    height: 44px;
    line-height: 44px;
    text-align: center;
    font-size: 14px;
    display: flex;
    a{
        display: block;
        width: 25%;
        color: hsla(0,0%,100%,.5);
    }
    a.active{
        color: #ffcd32;
        span{
            padding-bottom: 5px;
            border-bottom: 2px solid #ffcd32;
        }
    }
`