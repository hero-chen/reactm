// 导入库
import React, { Component } from 'react'

// 导入样式
import './static/css/reset.less';

// 导入路由
import Router from './router/index'

import { AppDiv } from './styled.js'

// 定义组件
class App extends Component  
{
 render() {
     return (
         <AppDiv>
             <Router />
         </AppDiv>
     )
 }
}

export default App