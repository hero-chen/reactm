// 导入库
import React from 'react';
import {render} from 'react-dom';

// 导入组件
import App from './App';

// 导入utils
import './utils/rem.js' 

// 渲染
render(
<App />,
document.getElementById('root')
);