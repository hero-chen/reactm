import React, { Component } from 'react'

// 导入样式
import { SearchDiv } from './styled'

// 导入公共组件
import Header from '../../components/header/index'
import Navs from '../../components/navs/index'

class Search extends Component {
 render() {
     return (
         <SearchDiv>
            <Header></Header>
            <Navs></Navs>
             this is Search index
         </SearchDiv>
     )
 }
}

export default Search