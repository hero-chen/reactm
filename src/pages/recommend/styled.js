import styled from 'styled-components'

export const RecommendDiv = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    .wrapper{
        flex: 1;
        // overflow-y: scroll;
        overflow: hidden;
    }
    .ulTop{
        width: 100%;
        height: 65px;
        line-height: 65px;
        text-align: center;
        font-size: 14px;
        color: #ffcd32;
    }
    ul{
        width: 100%;
        background:pink;
        li{
            padding: 0 20px 20px;
            height: 82px;
            width: 100%;
            box-sizing: border-box;
            display: flex;
            align-items: center;
            .l{
                width: 60px;
                height: 62px;
                margin-right: 20px;
                display: flex;
                img{
                    width: 60px;
                    height: 60px;
                }
            }
            .r{
                flex: 1;
                font-size: 14px;
                .title{
                    margin-bottom: 10px;
                    color: #fff;
                }
                .desc{
                    color: hsla(0,0%,100%,.3);
                }
            }
        }
    }
    .loading{
        height: 0.2rem;
    }
`