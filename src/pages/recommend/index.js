import React, { Component } from 'react'

// 导入样式
import { RecommendDiv } from './styled'

// 导入公共组件
import Header from '../../components/header/index'
import Navs from '../../components/navs/index'
import Lbt from '../../components/lbt/index'

// 引入滚动插件
import BScroll from 'better-scroll'

class Recommend extends Component {
    constructor(props){
        super(props)
        this.state = {
            pagenum: 1,
            musics: [
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
            ]
        }
    }

    componentDidMount() {
        const wrapper = document.querySelector('.wrapper')
        //选中DOM中定义的 .wrapper 进行初始化
        const scroll = new BScroll(wrapper, {
          pullUpLoad: {
            threshold: 5
        }
        })
        // 监控
        scroll.on('pullingUp', () => {
            console.log('触发上拉刷新啦~~')
            let pagenum = this.state.pagenum + 1
            console.log(`第${pagenum}页`)
            let oldMusics = this.state.musics
            // 发送异步请求
            let res = [
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
                {
                    img: '/icon.png',
                    title: '哈哈',
                    desc: '八的及时反馈'
                },
            ]
            let musics = oldMusics.concat(res)
            this.setState({
                pagenum,
                musics,
            })


            scroll.finishPullUp()
        })

      }

 render() {
     return (
         <RecommendDiv>
            <Header></Header>
            <Navs></Navs>
            <div className="wrapper">
                <div className="scroll">
                    <Lbt></Lbt>
                    <h1 className="ulTop">热门歌单推荐</h1>
                    <ul>
                        <li>
                            <div className="l">
                                <img src="/icon.png" alt="h" />
                            </div>
                            <div className="r">
                                <p className="title">史学界啊</p>
                                <p className="desc">哈克斯的罚款</p>
                            </div>
                        </li>
                        {
                            this.state.musics.map((item, index) => (
                                <li key={index}>
                                    <div className="l">
                                        <img src={item.img} alt={item.title} />
                                    </div>
                                    <div className="r">
                                        <p className="title">{item.title}</p>
                                        <p className="desc">{item.desc}</p>
                                    </div>
                                </li>
                            ))
                        }
                        
                    </ul>
                    <div>
                        <img src="/loading.gif" alt="loading" className="loading" />
                    </div>
                </div>
            </div>
            
         </RecommendDiv>
     )
 }
}

export default Recommend
