import React, { Component } from 'react'

// 导入样式
import { RankDiv } from './styled'

// 导入公共组件
import Header from '../../components/header/index'
import Navs from '../../components/navs/index'

class Rank extends Component {
 render() {
     return (
         <RankDiv>
            <Header></Header>
            <Navs></Navs>
             this is Rank index
         </RankDiv>
     )
 }
}

export default Rank