import React, { Component } from 'react'

// 导入样式
import { SingerdDiv } from './styled'

// 导入公共组件
import Header from '../../components/header/index'
import Navs from '../../components/navs/index'

class Singer extends Component {
 render() {
     return (
         <SingerdDiv>
            <Header></Header>
            <Navs></Navs>
             this is Singer index
         </SingerdDiv>
     )
 }
}

export default Singer