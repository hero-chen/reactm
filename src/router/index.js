// 导入库
import React, {Component} from 'react';
import {
BrowserRouter as Router,
Route,
Switch,
Redirect
} from 'react-router-dom';

// 导入路由配置
import routes from './routes';

// 定义路由
class ReactRouter extends Component
{
render() 
{
  return (
      <Router>
          <Switch>
              {/* 路由遍历 start */}
              {routes.map((item, i) => (
                  <Route
                      key={item.key || i}
                      path={item.path}
                      exact={item.exact}
                      render={(props) => {
                         // 检测登录 
                         const token = localStorage.getItem('token')
                         if (item.islogin)
                         {
                             if (token){ // 如果登录了, 返回正确的路由
                                 return <item.component {...props} {...item.extraProps} />
                             } else { // 没有登录就重定向至登录页面
                                 return <Redirect to='/login'/> 
                             }
                         } else {
                             return <item.component {...props} {...item.extraProps} />
                         }
                      }}
                  />
              ))}
              {/* 路由遍历 end */}
          </Switch> 
      </Router>
  )
} 
}

export default ReactRouter;